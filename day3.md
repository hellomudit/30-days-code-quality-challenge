## Get Rid of a Warning

## Exercise
Get rid of a warning you've gotten used to ignoring.

Chances are, your code prints at least one warning during one of the
following:

- Booting your code
- Running your tests
- Installing dependencies
- Deploying your app

Try to nuke of at least one warning. See if you can eliminate a 
[broken window](https://blog.codinghorror.com/the-broken-window-theory/).

If you see none, congrats! 

## Why do this?

Warnings are easy to learn to ignore with time, but there are two
problems with ignoring warning:

1. Many warnings (like a deprecation warning) will turn into full-blown
   issues later on. This will tend to happen at inconvenient times.

2. They act as a "broken window": an indication that your codebase isn't
   receiving the fastidious love and care it needs. These small messes
quietly demonstrate to your team that messes are tolerated, and can
often result in more.

